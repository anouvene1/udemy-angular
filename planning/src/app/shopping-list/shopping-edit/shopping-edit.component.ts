import { Component, OnInit } from '@angular/core';

import { Ingredient } from 'src/app/shared/ingredient.model';
import { IngredientService } from 'src/app/shared/ingredient.service';

@Component({
  selector: 'plan-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.scss']
})
export class ShoppingEditComponent implements OnInit {
  ingredient: Ingredient;

  constructor(private ingredientService: IngredientService ) { }

  ngOnInit() {
    this.ingredient = new Ingredient("", null);
  }

  addIngredient(ingredient: Ingredient) {
    console.log("Ingrédient ajouté avec succès: ", this.ingredientService.insertIngredient(ingredient));
  }
}
