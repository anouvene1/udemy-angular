import { Component, OnInit } from "@angular/core";
import { Ingredient } from '../shared/ingredient.model';
import { IngredientService } from '../shared/ingredient.service';

@Component({
    selector: "plan-shopping-list",
    templateUrl: "shopping-list.component.html",
    styleUrls: ["shopping-list.component.scss"]
})
export class ShoppingListComponent implements OnInit {
    ingredients: Ingredient[] = [];

    constructor(private ingredientService: IngredientService) {}

    ngOnInit(): void {
        this.ingredients = this.ingredientService.retreiveAllIngredients();
    }

    trackByItem(id, item) {
        console.log(item);
    }
}