import { Injectable } from '@angular/core';
import { Ingredient } from './ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  ingredients: Ingredient[] = [
    {name: 'Apples', amount: 5},
    {name: 'Tomatoes', amount: 2},
    {name: 'Cheese', amount: 1}
  ];

  constructor() { }

  retreiveAllIngredients(): Ingredient[] {
    return this.ingredients;
  }

  insertIngredient(ingredient: Ingredient): number {
    return this.ingredients.push(ingredient);
  }

}
