import { Directive, ElementRef, Renderer2, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[planSelectedRecipe]'
})
export class SelectedRecipeDirective {

  constructor(private elemRef: ElementRef, private render: Renderer2) { }

  @HostBinding('class.selectedRecipe') isSelected = false;
  @HostListener('mouseenter') onMouseenterRecipe() {
    this.elemRef.nativeElement.querySelector('.btn').addEventListener('click', () => {
      this.render.removeClass(
        document.querySelector('#recipesList').querySelectorAll('.btn'), 
        'selectedRecipe'
      );
      this.isSelected = true;
    });
  }

}
