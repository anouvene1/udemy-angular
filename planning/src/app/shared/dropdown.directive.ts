import { Directive, HostListener, ElementRef, Renderer2, HostBinding } from '@angular/core';

@Directive({
  selector: '[planDropdown]'
})
export class DropdownDirective {

  // private toggleDropdown: boolean = false;

  constructor(private elem: ElementRef, private render: Renderer2) { }

  @HostBinding('class.open') isOpen = false;

  @HostListener('click') onClick() {
    this.isOpen = !this.isOpen;

    // if(!this.toggleDropdown) {
    //   this.elem.nativeElement.nextSibling.style.display = 'block';
    //   this.toggleDropdown = true;
    // } else {
    //   this.elem.nativeElement.nextSibling.style.display = 'none';
    //   this.toggleDropdown = false;
    // }
    
  }

  @HostListener('mouseleave') onMouseleave() {
    this.elem.nativeElement.querySelector('.dropdown-menu').addEventListener('mouseleave', ()=> {
      // this.elem.nativeElement.nextSibling.style.display = 'none';
      // this.toggleDropdown = false;

      this.isOpen = false;
    });
  }

}
