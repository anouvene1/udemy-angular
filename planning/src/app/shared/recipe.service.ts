import { Injectable } from '@angular/core';
import { Recipe } from '../recipes/recipe-list/recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  recipes: Recipe[] = [
    {id: 1, name: "Marguerita", description: "Pizza is so good", imagePath: "https://cdn.pixabay.com/photo/2017/09/19/20/19/pizza-2766471_960_720.jpg"},
    {id: 2, name: "Bo Bun", description: "Nouille boeuf et vermicelles - Vietnam", imagePath: "https://cdn.pixabay.com/photo/2016/05/09/10/26/rice-1381146_960_720.jpg"},
    {id: 3, name: "Salade Greque", description: "Pizza is so good", imagePath: "https://cdn.pixabay.com/photo/2018/04/21/03/47/food-3337621_960_720.jpg"},
    {id: 4, name: "Hamburger", description: "Pain noir", imagePath: "https://cdn.pixabay.com/photo/2017/10/16/09/01/hamburger-2856548_960_720.jpg"}
  ]

  constructor() { }

  retreiveAllRecipes(): Recipe[] {
    return this.recipes;
  } 


}
