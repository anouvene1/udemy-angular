import { Directive, ElementRef, Renderer2, OnInit, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[planOddColor]'
})
export class OddColorDirective implements OnInit {

  constructor(private elem: ElementRef, private render: Renderer2) { }

  ngOnInit(): void {
    this.render.setStyle(this.elem.nativeElement, 'background-color', '#FAFAFA');
  }

  @Input('planOddColor') backgroundHover: string = 'url("assets/images/cook-1.webp")';
  
  @HostBinding('class') italic: string = 'list-group-item list-group-item-action d-flex justify-content-between align-items-center';

  @HostListener('mouseenter') onMouseEnter()  {
    this.elem.nativeElement.style.fontWeight = 'bold';
    this.elem.nativeElement.style.color = '#007bff';
    this.elem.nativeElement.style.textAlign = 'center';
    // Add italicTxt class
    this.italic = "list-group-item list-group-item-action align-items-center italicTxt"
    
    // Add background image
    this.elem.nativeElement.style.backgroundImage = this.backgroundHover;
  }

  @HostListener('mouseleave') onMouseLeave()  {
    this.elem.nativeElement.style.fontWeight = 'normal';
    this.elem.nativeElement.style.color = '#495057';
    this.elem.nativeElement.style.textAlign = 'left';

    // Remove italicTxt class
    this.italic = "list-group-item list-group-item-action d-flex justify-content-between align-items-center"
    
    // remove background image
    this.elem.nativeElement.style.backgroundImage = "";
  }

  

}
