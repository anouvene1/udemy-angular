import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'plan-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output('emittedUrl') url: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  gotoRecipe(url: string) {
    this.url.emit(url);
  }

}
