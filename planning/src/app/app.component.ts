import { Component } from '@angular/core';

@Component({
  selector: 'plan-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'planning';
  selectedRecipeUrl = "recipes";

  navigateOn($event) {
    this.selectedRecipeUrl = $event;
  }
}
