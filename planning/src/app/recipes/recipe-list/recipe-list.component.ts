import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Recipe } from './recipe.model';
import {  } from 'events';
import { RecipeService } from 'src/app/shared/recipe.service';

@Component({
  selector: 'plan-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [];
  
  

  @Output() emittedRecipeDetail: EventEmitter<Recipe> = new EventEmitter();
  
  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
    this.recipes = this.recipeService.retreiveAllRecipes();
  }

  getRecipeDetail(recipe: Recipe) {
    this.emittedRecipeDetail.emit(recipe);
  }

  trackById(id, recipe) {
    return recipe;
  }

}
