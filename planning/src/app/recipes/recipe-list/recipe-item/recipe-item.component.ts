import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'plan-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.scss']
})
export class RecipeItemComponent implements OnInit {
  shortDescription = "";
  @Input() recipe: Recipe;
  @Input() selectedRecipe: boolean;
  @Output() emittedRecipe: EventEmitter<Recipe> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.shortDescription = this.recipe.description.substring(0, 50) + "...";
  }

  showDetails(recipe: Recipe) {
    this.emittedRecipe.emit(recipe);
  }

}
