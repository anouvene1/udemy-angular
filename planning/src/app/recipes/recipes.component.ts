import { Component } from '@angular/core';
import { Recipe } from './recipe-list/recipe.model';

@Component({
    selector: "plan-recipes",
    templateUrl: "recipes.component.html",
    styleUrls: ["recipes.component.scss"]
})
export class RecipesComponent {
    recipeDetail: Recipe;

    showRecipeDetail(recipe: Recipe) {
        this.recipeDetail = recipe;
    }
}